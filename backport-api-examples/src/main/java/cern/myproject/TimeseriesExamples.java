package cern.myproject;

import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.client.service.TimeseriesDataService;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatistics;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatisticsSet;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.common.collect.ImmutableList;
import org.apache.hadoop.security.UserGroupInformation;

import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

public class TimeseriesExamples {
    static {
        // Uncomment in order to overwrite the default security settings.
        //System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");

        // AUTHENTICATION:
        // uncomment set these values to use keytab, leave commented to use shared state token (must run kinit first)

        // System.setProperty("kerberos.principal", "nxcalsuser");
        // System.setProperty("kerberos.keytab", "/opt/nxcalsuser/.keytab");
        // NXCALS PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094");

    }

    public static void main(String[] args) throws IOException {
        if (System.getProperty("user.name") != null && System.getProperty("user.password") != null) {
            System.out.println("Using RBAC authentication to login");
            loginWithRbac();
        } else if (System.getProperty("kerberos.principal") != null && System.getProperty("kerberos.keytab") != null) {
            System.out.println("Using keytab to login");
            UserGroupInformation.loginUserFromKeytab(System.getProperty("kerberos.principal"), System.getProperty("kerberos.keytab"));
        } else {
            System.out.println("Using shared state token to login");
        }

        getDataForMultipleVariablesAggregatedInFixedIntervals();
        getDataAlignedToTimestamps();
        getDataInTimeWindowFilteredByFundamentals();
        getDataInTimeWindow();
        getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval();
        getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval();
        getDataFilteredByTimestamps();
        getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval();
        getDataInTimeWindowFilteredByValues();
        getDataInFixedIntervals();
        getVariableStatisticsOverMultipleVariablesInTimeWindow();
        getDataHavingMaxValueInTimeWindow();
        getVectornumericDataInTimeWindowFilteredByVectorIndices();
        getLastDataPriorToTimestampWithinDefaultInterval();
        getLastDataPriorToTimestampWithinUserInterval();
        getLastDataPriorToNowWithinDefaultInterval();
        getLastDataPriorToNowWithinUserInterval();
        getNextDataAfterTimestampWithinDefaultInterval();
        getNextDataAfterTimestampWithinUserInterval();
        getDataForVariableAggregated();
        getMultiColumnDataAlignedToTimestamps();
        getMultiColumnDataInTimeWindow();
        getMultiColumnDataInFixedIntervals();
        getJVMHeapSizeEstimationForDataInTimeWindow();
        getMaxVectornumericElementCountInTimeWindow();
        getDataForSnapshot1();
        getDataForSnapshot2();
        getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern();
        getDataDistribution();
        getDataDistributionInTimewindowForBinsNumber();
        getVariableStatisticsOverTimeWindowFilteredByFundamentals();
        getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFiltered();
    }

    public static void getDataForMultipleVariablesAggregatedInFixedIntervals() {
        // TODO: unimplemented
    }

    public static void getDataAlignedToTimestamps() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet drivingSet = timeseriesDataService.getDataInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"));

        TimeseriesDataSet data = timeseriesDataService.getDataAlignedToTimestamps(variable, drivingSet);

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataInTimeWindowFilteredByFundamentals() {
        // TODO: unimplemented
    }

    public static void getDataInTimeWindow() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"));

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000")
        );

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"),
                LoggingTimeInterval.YEAR
        );

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataFilteredByTimestamps() {
        // TODO: unimplemented
    }

    public static void getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval() {
        // TODO: unimplemented
    }

    public static void getDataInTimeWindowFilteredByValues() {
        // TODO: unimplemented
    }

    public static void getDataInFixedIntervals() {
        // TODO: unimplemented
    }

    public static void getVariableStatisticsOverMultipleVariablesInTimeWindow() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");
        VariableSet variableSet = new VariableSet(variable);


        VariableStatisticsSet data = timeseriesDataService.getVariableStatisticsOverMultipleVariablesInTimeWindow(variableSet,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000")
        );

        printStatistics(data);
    }

    public static void getDataHavingMaxValueInTimeWindow() {
        // TODO: unimplemented
    }

    public static void getVectornumericDataInTimeWindowFilteredByVectorIndices() {
        // TODO: unimplemented
    }

    public static void getLastDataPriorToTimestampWithinDefaultInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getLastDataPriorToTimestampWithinDefaultInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"));

        System.out.println("Last value before 2018-06-19 00:05:00.000");
        printDatapoint(data);
    }

    public static void getLastDataPriorToTimestampWithinUserInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getLastDataPriorToTimestampWithinUserInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                LoggingTimeInterval.DAY
        );

        if (data == null) {
            System.out.println("No data at most one day before 2018-06-19 00:05:00.000");
        }

        data = timeseriesDataService.getLastDataPriorToTimestampWithinUserInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"),
                LoggingTimeInterval.DAY
        );

        System.out.println("Last value at most one day before 2019-04-09 00:00:00.000");
        printDatapoint(data);
    }

    public static void getLastDataPriorToNowWithinDefaultInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getLastDataPriorToNowWithinDefaultInterval(variable);

        System.out.println("Last value at most one day before now");
        printDatapoint(data);
    }

    public static void getLastDataPriorToNowWithinUserInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getLastDataPriorToNowWithinUserInterval(variable, LoggingTimeInterval.DAY);

        if (data == null) {
            System.out.println("No data at most one day before now");
        }
    }

    public static void getNextDataAfterTimestampWithinDefaultInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getNextDataAfterTimestampWithinDefaultInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"));

        System.out.println("Next value after 2018-06-19 00:05:00.000");
        printDatapoint(data);
    }

    public static void getNextDataAfterTimestampWithinUserInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesData data = timeseriesDataService.getNextDataAfterTimestampWithinUserInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                LoggingTimeInterval.DAY
        );

        if (data == null) {
            System.out.println("No data at most one day before 2018-06-19 00:00:00.000");
        }

        data = timeseriesDataService.getNextDataAfterTimestampWithinUserInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"),
                LoggingTimeInterval.DAY
        );

        System.out.println("Last value at most one day before 2018-06-19 00:05:00.000");
        printDatapoint(data);
    }

    public static void getDataForVariableAggregated() {
        // TODO: unimplemented
    }

    public static void getMultiColumnDataAlignedToTimestamps() {
        // TODO: unimplemented
    }

    public static void getMultiColumnDataInTimeWindow() {
        // TODO: unimplemented
    }

    public static void getMultiColumnDataInFixedIntervals() {
        // TODO: unimplemented
    }

    public static void getJVMHeapSizeEstimationForDataInTimeWindow() {
        // TODO: unimplemented
    }

    public static void getMaxVectornumericElementCountInTimeWindow() {
        // TODO: unimplemented
    }

    public static void getDataForSnapshot1() {
        // TODO: unimplemented
    }

    public static void getDataForSnapshot2() {
        // TODO: unimplemented
    }

    public static void getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern() {
        // TODO: unimplemented
    }

    public static void getDataDistribution() {
        // TODO: unimplemented
    }

    public static void getDataDistributionInTimewindowForBinsNumber() {
        // TODO: unimplemented
    }

    public static void getVariableStatisticsOverTimeWindowFilteredByFundamentals() {
        // TODO: unimplemented
    }

    public static void getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFiltered() {
        // TODO: unimplemented
    }

    private static void printData(TimeseriesDataSet data) {
        data.forEach(TimeseriesExamples::printDatapoint);
    }

    private static void printDatapoint(TimeseriesData dataPoint) {
        try {
            if (dataPoint == null) {
                System.out.println("Absent data");
            } else {
                System.out.println(dataPoint.getStamp() + ": " + dataPoint.getDoubleValue());
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private static void printStatistics(VariableStatisticsSet statisticsSet) {
        Iterator<VariableStatistics> iterator = statisticsSet.iterator();
        while (iterator.hasNext()) {
            final VariableStatistics next = iterator.next();
            System.out.println("Values for variable : " + next.getVariableName() + " max: " + next.getMaxValue() + " min: " + next.getMinValue());
        }
    }

    private static Variable getVariable(String variableName) {
        return ServiceBuilder.getInstance().createMetaService()
                .getVariablesWithNameInListofStrings(ImmutableList.of(variableName)).iterator().next();
    }

    private static void loginWithRbac() {
        // Enable NXCALS RBAC authentication in Hadoop delegation tokens generation, in Spark (for extraction)
        System.setProperty("NXCALS_RBAC_AUTH", "true");

        //Login with RBAC, must set user.password or give it explicitly here, don't forget to remove before commit!
        loginWithRbac(System.getProperty("user.name"), System.getProperty("user.password"));
    }

    private static void loginWithRbac(String user, String password) {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }
}

package cern.myproject;

import cern.nxcals.api.backport.client.service.MetaDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElementsSet;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.common.collect.ImmutableList;
import org.apache.hadoop.security.UserGroupInformation;

import java.io.IOException;
import java.util.Optional;
import java.util.StringJoiner;

public class MetadataExamples {
    static {
        // Uncomment in order to overwrite the default security settings.
        //System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");

        // AUTHENTICATION:
        // uncomment set these values to use keytab, leave commented to use shared state token (run kinit first)

        // System.setProperty("kerberos.principal", "nxcalsuser");
        // System.setProperty("kerberos.keytab", "/opt/nxcalsuser/.keytab");

        // NXCALS PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094");

    }

    public static void main(String[] args) throws IOException {
        if (System.getProperty("user.name") != null && System.getProperty("user.password") != null) {
            System.out.println("Using RBAC authentication to login");
            loginWithRbac();
        } else if (System.getProperty("kerberos.principal") != null && System.getProperty("kerberos.keytab") != null) {
            System.out.println("Using keytab to login");
            UserGroupInformation.loginUserFromKeytab(System.getProperty("kerberos.principal"),
                    System.getProperty("kerberos.keytab"));
        } else {
            System.out.println("Using shared state token to login");
        }

        getVariablesOfDataTypeAttachedToHierarchy();
        getVariablesOfDataTypeWithNameLikePatternAttachedToHierarchy();
        getVariablesOfDataTypeInVariableList();
        getFundamentalsInTimeWindowWithNameLikePattern();
        getVariablesOfDataTypeWithNameLikePattern();
        getVariablesWithNameInListofStrings();
        getVectorElements();
        getVectorElementsInTimeWindow();
        getAllHierarchies();
        getHierarchyChildNodes();
        getHierarchyForNodePath();
        getTopLevelHierarchies();
        getHierarchiesForVariable();
        getJapcParameterDefinitionForDeviceAndPropertyName();
        getVariablesOfDataTypeWithNameLikePatternInVariableList();
        getAllDeviceNames();
        getPropertyNamesForDeviceName();
        getJapcDefinitionFor();
    }

    public static void getVariablesOfDataTypeAttachedToHierarchy() {
        // TODO: unimplemented
    }

    public static void getVariablesOfDataTypeWithNameLikePatternAttachedToHierarchy() {
        // TODO: unimplemented
    }

    public static void getVariablesOfDataTypeInVariableList() {
        // TODO: unimplemented
    }

    public static void getFundamentalsInTimeWindowWithNameLikePattern() {
        // TODO: unimplemented
    }

    public static void getVariablesOfDataTypeWithNameLikePattern() {
        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();

        VariableSet variables = metaDataService.getVariablesOfDataTypeWithNameLikePattern("PR.DCAFTINJ_1:I%",
                VariableDataType.ALL);
        variables.forEach(MetadataExamples::printVariable);
    }

    public static void getVariablesWithNameInListofStrings() {
        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();

        VariableSet variables = metaDataService.getVariablesWithNameInListofStrings(
                ImmutableList.of("DQQDS.B15R3.RQF.A34:ST_PWR_PERM", "DQQDS.B15R3.RQF.A34:U_REF_N1"));
        variables.forEach(MetadataExamples::printVariable);
    }

    private static void printVariable(Variable v) {
        System.out.println(
                "Found Variable:" + v.getVariableName() + " of type " + v.getVariableDataType() + " and system "
                        + v.getSystem());
    }

    public static void getVectorElements() {
        final String variableName = "LHC.BLMI:LOSS_RS01";

        StringBuilder outputMsg = new StringBuilder();

        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();
        Variable variable = getVariable(variableName);
        VectornumericElementsSet vectornumericElementsSet = metaDataService.getVectorElements(variable);

        outputMsg.append("Listing number of elements for all the contexts attached to " + variableName + ":\n");
        vectornumericElementsSet.getVectornumericElements().forEach((key, value) -> {
            outputMsg.append(key).append(": ").append(value.size()).append("\n");
        });

        System.out.println(outputMsg.toString());
    }

    public static void getVectorElementsInTimeWindow() {
        final String variableName = "LHC.BLMI:LOSS_RS02";

        StringBuilder outputMsg = new StringBuilder();

        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();
        Variable variable = getVariable(variableName);
        VectornumericElementsSet vectornumericElementsSet = metaDataService.getVectorElementsInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2016-01-01 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2017-01-01 00:00:00.000"));

        outputMsg.append("Listing first 10 elements for contexts attached to " + variableName + " in 2016:\n");
        vectornumericElementsSet.getVectornumericElements().forEach((key, value) -> {
            outputMsg.append(key).append(": ");

            StringJoiner elements = new StringJoiner(",");
            for (int idx = 0; idx < value.size() && idx < 10; idx++) {
                elements.add(value.getElementName(idx));
            }
            outputMsg.append(elements).append("\n");
        });

        System.out.println(outputMsg.toString());
    }

    public static void getAllHierarchies() {
        // TODO: unimplemented
    }

    public static void getHierarchyChildNodes() {
        // TODO: unimplemented
    }

    public static void getHierarchyForNodePath() {
        // TODO: unimplemented
    }

    public static void getTopLevelHierarchies() {
        // TODO: unimplemented
    }

    public static void getHierarchiesForVariable() {
        // TODO: unimplemented
    }

    public static void getJapcParameterDefinitionForDeviceAndPropertyName() {
        // TODO: unimplemented
    }

    public static void getVariablesOfDataTypeWithNameLikePatternInVariableList() {
        // TODO: unimplemented
    }

    public static void getAllDeviceNames() {
        // TODO: unimplemented
    }

    public static void getPropertyNamesForDeviceName() {
        // TODO: unimplemented
    }

    public static void getJapcDefinitionFor() {
        // TODO: unimplemented
    }

    private static Variable getVariable(String variableName) {

        return Optional.ofNullable(ServiceBuilder.getInstance().createMetaService()
                        .getVariablesWithNameInListofStrings(ImmutableList.of(variableName))
                        .getVariable(variableName))
                .orElseThrow(() -> new IllegalArgumentException("Could not find: " + variableName));
    }

    private static void loginWithRbac() {
        //Login with RBAC, must set user.password or give it explicitly here, don't forget to remove before commit!
        loginWithRbac(System.getProperty("user.name"), System.getProperty("user.password"));
    }

    private static void loginWithRbac(String user, String password) {
        System.setProperty("NXCALS_RBAC_AUTH", "true");
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }
}

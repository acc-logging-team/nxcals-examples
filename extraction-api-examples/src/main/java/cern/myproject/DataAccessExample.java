/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.myproject;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This is the minimal NXCALS Spark data access application.
 * Please note that you need first to ask for access to the data by sending e-mail to acc-logging-support@cern.ch.
 * Later you need to have a valid Kerberos token in order to access the IT Hadoop cluster where the data is located.
 * NOTE: THIS IS WORK IN PROGRESS, API MIGHT CHANGE!
 */
@SpringBootApplication
@Import(SparkContext.class)
public class DataAccessExample {

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
        // Logging debug if necessary
        // System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");

        // Uncomment in order to overwrite the default security settings.
        //System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");

        // Uncomment to provide principal and path to your keytab, by default the process acquires that info
        // from the locally cached kerberos ticket
        //System.setProperty("kerberos.principal", "nxcalsuser");
        //System.setProperty("kerberos.keytab", "/opt/nxcalsuser/.keytab");

        //In Spark properties this is also needed for yarn:
        //System.setProperty("spark.kerberos.principal", "nxcalsuser");
        //System.setProperty("spark.kerberos.keytab", "/opt/nxcalsuser/.keytab");

        // NXCALS PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094");


    }
    private static final Logger log = LoggerFactory.getLogger(DataAccessExample.class);
    private static final String SYSTEM_NAME = "CMW";

    public static void main(String[] args) {
        // Uncomment to login with RBAC authentication or provide Kerberos ticket (see above)
        // loginWithRbac();

        ConfigurableApplicationContext context = SpringApplication.run(DataAccessExample.class, args);

        // Loads SparkSession that runs on the local machine (no executors on the cluster).
        // It is quite efficient for smaller queries, getting data from last few hours, etc.
        // You can alter {@link SparkSession} configuration from application.yml properties file
        SparkSession spark = context.getBean(SparkSession.class);

        DataAccessExample dataAccessExample = new DataAccessExample();

        log.info("############## Lets get some cmw data ############");
        dataAccessExample.getCmwData(spark);

        log.info("############## Lets get some variable data ############");
        dataAccessExample.getVariableDataForHierarchy(spark, "/EXAMPLE");

        log.info("THE END, Happy Exploring Spark API!");
    }

    List<Long> getCmwData(SparkSession spark) {

        List<Long> datasetSizes = new ArrayList<>();

        String startTime = "2018-08-01 00:00:00.00";
        String endTime = "2018-08-01 01:00:00.00";

        //This is the meta-data query
        Dataset<Row> exampleDataset = DevicePropertyDataQuery.builder(spark).system(SYSTEM_NAME)
                .startTime(startTime).endTime(endTime).entity()
                .parameter("SPSBQMSPSv1/Acquisition").buildDataset();


        log.info("What are the fields available?");
        exampleDataset.printSchema();

        log.info("Some timing data here:");
        exampleDataset.select("cyclestamp","selector","bunchIntensityMean").show();

        long nrOfRecordsinExampleDataset = exampleDataset.count();
        log.info("Let's see how many records data were submitted during that time: {} ", nrOfRecordsinExampleDataset);
        datasetSizes.add(nrOfRecordsinExampleDataset);

        log.info("Basic statistics about the intensity (bunchIntensityMean), like count, mean, stddev, min, max:");
        exampleDataset.describe("bunchIntensityMean").show();

        exampleDataset.createOrReplaceTempView("myData");
        log.info("Run an SQL statement to calculate average intensity for TOF user: SELECT avg(bunchIntensityMean) FROM myData WHERE selector == 'SPS.USER.LHC1'");

        spark.sql("select avg(bunchIntensityMean) from myData where selector == 'SPS.USER.LHC1' ").show();

        //TGM data to extract destination
        Dataset<Row> tgmData = DevicePropertyDataQuery.builder(spark).system(SYSTEM_NAME).startTime(startTime).endTime(endTime)
                .entity().parameter("SPS.TGM/FULL-TELEGRAM.STRC").buildDataset();
        datasetSizes.add(tgmData.count());

        log.info("What are the fields available?");
        tgmData.printSchema();

        log.info("Printing out data");
        tgmData.select("cyclestamp","USER","DEST").show();

        log.info("Join data to show only for a destination == LHC");
        exampleDataset.join(tgmData, "cyclestamp").where("DEST = 'LHC' and bunchIntensityMean is not null").select("cyclestamp","bunchIntensityMean", "DEST").show();

        return datasetSizes;
    }

    List<Long> getVariableDataForHierarchy(SparkSession spark, String hierarchyPath) {
        HierarchyService service = ServiceClientFactory.createHierarchyService();
        List<Long> datasetSizes = new ArrayList<>();

        log.info("Getting hierarchy for {}", hierarchyPath);
        Hierarchy node = service.findOne(Hierarchies.suchThat().path().eq(hierarchyPath))
                .orElseThrow(()->new IllegalArgumentException("No such hierarchy path " + hierarchyPath));

        log.info("Found hierarchy: {}", node.getNodePath());

        String startTime = "2018-06-19 00:00:00.000"; //UTC
        String endTime = "2018-06-19 00:10:00.000"; //UTC

        Set<Variable> variables = service.getVariables(node.getId());
        for (Variable variableData: variables) {
            log.info("Querying for {} variable between {} and {}", variableData.getVariableName(), startTime, endTime);
            Dataset<Row> dataset = DataQuery.builder(spark).byVariables().system("CMW").startTime(startTime)
                    .endTime(endTime).variable(variableData.getVariableName()).buildDataset();

            long datasetSize = dataset.count();
            datasetSizes.add(datasetSize);
            log.info("Got {} rows for {}", datasetSize, variableData.getVariableName());
            dataset.show();
        }

        return datasetSizes;
    }

    private static void loginWithRbac() {
        // Enable NXCALS RBAC authentication in Hadoop delegation tokens generation, in Spark (for extraction)
        System.setProperty("NXCALS_RBAC_AUTH", "true");

        //Login with RBAC, must set user.password or give it explicitly here, don't forget to remove before commit!
        loginWithRbac(System.getProperty("user.name"), System.getProperty("user.password"));
    }

    private static void loginWithRbac(String user, String password) {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }
}

package cern.myproject;

import org.apache.spark.sql.SparkSession;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataAccessExample.class)
public class DataAccessExampleTest {

    @Autowired
    private DataAccessExample dataAccessExample;

    @Autowired
    private SparkSession spark;

    @Test
    public void shouldRetrieveSameNrOfRecordsForCmw() {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(61L, 640L)), dataAccessExample.getCmwData(this.spark));
    }
}

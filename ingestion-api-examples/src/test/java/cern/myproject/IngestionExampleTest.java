package cern.myproject;

import org.junit.Assert;
import org.junit.Test;

public class IngestionExampleTest {
    private static final long NR_OF_MESSAGES = 5L;

    @Test
    public void shouldPublishMessages() {

        IngestionExample ingestionExample = new IngestionExample();

        long nrSent = ingestionExample.runExample(NR_OF_MESSAGES);

        Assert.assertEquals(NR_OF_MESSAGES, nrSent);
    }
}

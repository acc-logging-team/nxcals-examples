package cern.myproject;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.ingestion.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

/**
 * NXCALS Ingestion API Example. It sends {@link MAX_NUMBER_OF_MSG_TO_BE_SENT} messages to NXCALS TestBed environment
 * in an asynchronous way.
 * <p>
 * Please note that you need to be authenticated to the relevant NXCALS service (currently using Kerberos token)
 * in order to be able to send any messages to NXCALS.
 * Additionally you also must be authorized to read {@link SYSTEM_NAME} configuration - please contact
 * <a href="mailto:acc-logging-support@cern.ch">Logging Support</a> to obtain the necessary rights.
 * </p>
 * If you have any questions related to the example please contact the aforementioned Logging Support address.
 */
public class IngestionExample {

    private static final Logger LOGGER = LoggerFactory.getLogger(IngestionExample.class);

    private static final String SYSTEM_NAME = "MOCK-SYSTEM";
    // we send data to NXCALS in asynchronous way using the below thread pool
    private final ExecutorService ingestionExecutor = new ThreadPoolExecutor(1, 5, 2, TimeUnit.MINUTES,
            new ArrayBlockingQueue<>(100), new ThreadPoolExecutor.CallerRunsPolicy());
    // just for test
    private static final long MAX_NUMBER_OF_MSG_TO_BE_SENT = 200L;

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");

        // Uncomment in order to overwrite the default security settings.
        //System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");

        // Uncomment to provide principal and path to your keytab, by default the process acquires that info
        // from the locally cached kerberos ticket
        //System.setProperty("kerberos.principal", "nxcalsuser");
        //System.setProperty("kerberos.keytab", "/opt/nxcalsuser/.keytab");

        // NXCALS Testbed (for PRO access please contact Logging team!)
        System.setProperty("service.url",
                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed2.cern.ch:19094,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093,https://cs-ccr-nxcalstbs2.cern.ch:19093");
        System.setProperty("kafka.producer.bootstrap.servers",
                "cs-ccr-nxcalstbs1.cern.ch:9092,cs-ccr-nxcalstbs2.cern.ch:9092,cs-ccr-nxcalstbs3.cern.ch:9092,cs-ccr-nxcalstbs4.cern.ch:9092");
    }

    public static  void main(String[] args) {
        IngestionExample ingestionExample = new IngestionExample();
        ingestionExample.runExample(MAX_NUMBER_OF_MSG_TO_BE_SENT);
    }

    long runExample(long nrOfMessagesToSent) {
        AtomicLong nrOfMessagesSent = new AtomicLong(0);

        try (Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM_NAME, Function.identity())) {

            LOGGER.info("Will try to publish {} messages with data records, via the NXCALS client publisher",
                    nrOfMessagesToSent);

            List<CompletableFuture<Result>> confirmations = new ArrayList<>();

            for (int i = 0; i < nrOfMessagesToSent; i++) {
                int msgNum = i + 1;

                // get example data to be sent
                ImmutableData exampleData = getExampleData();

                // send data asynchronously to NXCALS
                LOGGER.info("Trying to publish message {}", i);
                CompletableFuture<Result> resultCallback = publisher.publishAsync(exampleData, ingestionExecutor);
                // once it has been sent we should get called via the returned callback of which
                // either result of exception will be not null
                resultCallback.handle((result, exception) -> {
                    // if we get exception means that something bad happened with our record (i.e. we used wrong
                    // timestamp <timestamp = 0>)
                    if (exception != null) {
                        LOGGER.error("Something bad happened while sending data {}", exampleData.getEntry("timestamp").get(),
                                exception);
                    } else {
                        LOGGER.info("Published record for message #{} with timestamp: {}",
                                msgNum, exampleData.getEntry("timestamp").get());
                        nrOfMessagesSent.incrementAndGet();
                    }
                    return result;
                });

                confirmations.add(resultCallback);
            }

            //wait for all confirmations
            CompletableFuture.allOf(confirmations.toArray(new CompletableFuture[] {})).join();
            ingestionExecutor.shutdown();
            LOGGER.info("Finished!");
        } catch (Exception ex) {
            LOGGER.error("We cannot send data to NXCALS for some reason", ex);
        }
        return nrOfMessagesSent.get();
    }

    private static ImmutableData getExampleData() {
        final long stampNanos = TimeUnit.NANOSECONDS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);

        DataBuilder builder = ImmutableData.builder();

        // entity key field as specified in the schema of the system definition
        builder.add("device", "device_test");

        // partition field based on the system declaration (used for data partitioning/indexing on the storage)
        builder.add("specification", "specification_test");

        // timestamp accordingly to the system (as pointed by the SYSTEM_NAME property) definition
        builder.add("timestamp", stampNanos);

        // user data (could be anything based on the actual use-case, since it's not enforced by the system)
        builder.add("array_field", new int[] { 1, 2, 3, 3 }, new int[] { 1 });
        builder.add("double_field", 123.6257D);
        builder.add("extra_time_field", stampNanos);
        builder.add("description", "This record produced by NXCALS examples");

        // some fields may be set to null, in such case we have to specify their type
        builder.addNull("selector", EntryType.STRING);

        return builder.build();
    }
}

package cern.myproject;

import org.junit.Assert;
import org.junit.Test;

public class ThinDataAccessExampleTest {

    private ThinDataAccessExample thinDataAccessExample = new ThinDataAccessExample();

    @Test
    public void shouldRetrieveSameNrOfRecordsForCmw() {
        ThinDataAccessExample.login();
        Assert.assertEquals(61, thinDataAccessExample.getCmwData().size());
    }
}

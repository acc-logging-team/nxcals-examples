/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.myproject;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * This is the minimal NXCALS Spark data access application.
 * Please note that you need first to ask for access to the data by sending e-mail to acc-logging-support@cern.ch.
 * Later you need to have a valid Kerberos token in order to access the IT Hadoop cluster where the data is located.
 */
@SpringBootApplication
public class MetadataAccessExample implements CommandLineRunner {

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
        // Logging debug if necessary
        // System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");

        // Uncomment to provide principal and path to your keytab, by default the process acquires that info
        // from the locally cached kerberos ticket
        //System.setProperty("kerberos.principal", "nxcalsuser");
        //System.setProperty("kerberos.keytab", "/opt/nxcalsuser/.keytab");

        // NXCALS PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094");
    }
    private static final Logger log = LoggerFactory.getLogger(MetadataAccessExample.class);

    @Autowired
    private VariableQueries variableQueries;

    @Autowired
    private EntityQueries entityQueries;

    public static void main(String[] args) {
        SpringApplication.run(MetadataAccessExample.class, args);
    }

    @Override
    public void run(String... args) {
        this.runVariableQueries();
        this.runEntityQueries();

        log.info("THE END, Happy Exploring Metadata API!");
    }

    private void runVariableQueries() {
        String nameQuery = "SPS%";
        List<Variable> variables;

        Consumer<Collection<Variable>> printer = (Collection<Variable> variablesCollection) -> variablesCollection.forEach(variable -> System.out.println(variable.getVariableName()));

        log.info("############## \"Warm-up\" - Get first 10 variables, that names start with PS, without configuration ############");
        variables = variableQueries.queryAndLimitThinVariables("PS%");
        printer.accept(variables);

        log.info("############## Get first 10 variables, that names start with SPS, without configuration ############");
        variables = variableQueries.queryAndLimitThinVariables(nameQuery);
        printer.accept(variables);

        log.info("############## Get first 10 variables, that names start with SPS, with configuration ############");
        variables = variableQueries.queryAndLimitThickVariables(nameQuery);
        printer.accept(variables);

        log.info("############## Lets get some variables, but sort them by name and get without configuration ############");
        variables = variableQueries.queryThinVariables(nameQuery);
        printer.accept(variables.subList(0, 10));

        log.info("############## Lets get some variables, old query ############");
        Set<Variable> variablesSet = variableQueries.queryVariables(nameQuery);
        System.out.println("Number of variables: " + variablesSet.size());
        printer.accept(variablesSet.stream().limit(10).collect(Collectors.toSet()));
    }

    private void runEntityQueries() {
        String nameQuery = "SPS%";
        List<Entity> entities;

        Consumer<Collection<Entity>> printer = (Collection<Entity> entitiesCollection) -> entitiesCollection.forEach(entity -> System.out.println(entity.getEntityKeyValues()));

        log.info("############## \"Warm-up\" - Get first 10 entities, that names start with PS, without history ############");
        entities = entityQueries.queryAndLimitEntitiesWithoutHistory("PS%");
        printer.accept(entities);

        log.info("############## Get first 10 entities, that names start with SPS, without history ############");
        entities = entityQueries.queryAndLimitEntitiesWithoutHistory(nameQuery);
        printer.accept(entities);

        log.info("############## Get first 10 entities, that names start with SPS, with history ############");
        entities = entityQueries.queryAndLimitEntitiesWithHistory(nameQuery);
        printer.accept(entities);

        log.info("############## Lets get some entities, but sort them by name and get without history ############");
        entities = entityQueries.queryEntitiesWithoutHistory(nameQuery);
        printer.accept(entities.subList(0, 10));

        log.info("############## Lets get some entities, old query ############");
        Set<Entity> entitiesSet = entityQueries.queryEntities(nameQuery);
        System.out.println("Number of entities: " + entitiesSet.size());
        printer.accept(entitiesSet.stream().limit(10).collect(Collectors.toSet()));
    }
}

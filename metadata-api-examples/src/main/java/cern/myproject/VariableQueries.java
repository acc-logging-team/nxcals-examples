package cern.myproject;

import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.api.metadata.queries.Variables;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class VariableQueries {
    private static final String SYSTEM_NAME = "CMW";
    private final VariableService variableService;
    VariableQueries() {
        variableService = ServiceClientFactory.createVariableService();
    }

    List<Variable> queryThinVariables(String nameQuery) {
        VariableQueryWithOptions queryWithOptions = Variables.suchThat().systemName().eq(SYSTEM_NAME).and().variableName().like(nameQuery).withOptions().noConfigs().orderBy().variableName().asc();
        return variableService.findAll(queryWithOptions);
    }

    List<Variable> queryAndLimitThinVariables(String nameQuery) {
        VariableQueryWithOptions queryWithOptions = Variables.suchThat().systemName().eq(SYSTEM_NAME).and().variableName().like(nameQuery).withOptions().noConfigs().orderBy().variableName().asc().limit(10);
        return variableService.findAll(queryWithOptions);
    }

    List<Variable> queryAndLimitThickVariables(String nameQuery) {
        VariableQueryWithOptions queryWithOptions = Variables.suchThat().systemName().eq(SYSTEM_NAME).and().variableName().like(nameQuery).withOptions().orderBy().variableName().asc().limit(10);
        return variableService.findAll(queryWithOptions);
    }

    Set<Variable> queryVariables(String nameQuery) {
        var query = Variables.suchThat().systemName().eq(SYSTEM_NAME).and().variableName().like(nameQuery);
        return variableService.findAll(query);
    }
}

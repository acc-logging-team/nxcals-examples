package cern.myproject;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class EntityQueries {
    private static final String SYSTEM_NAME = "CMW";
    private final SystemSpecService systemSpecService;
    private final EntityService entityService;
    EntityQueries() {
        systemSpecService = ServiceClientFactory.createSystemSpecService();
        entityService = ServiceClientFactory.createEntityService();
    }

    List<Entity> queryEntitiesWithoutHistory(String nameQuery) {
        SystemSpec cmwSystemSpec = systemSpecService.findByName(SYSTEM_NAME).orElseThrow();
        EntityQueryWithOptions queryWithOptions = Entities.suchThat()
                .systemName().eq(SYSTEM_NAME)
                .and().keyValues().like(cmwSystemSpec, Map.of(
                        "device", nameQuery, "property", "%"
                )).withOptions().noHistory().orderBy().id().asc();
        return entityService.findAll(queryWithOptions);
    }

    List<Entity> queryAndLimitEntitiesWithoutHistory(String nameQuery) {
        SystemSpec cmwSystemSpec = systemSpecService.findByName(SYSTEM_NAME).orElseThrow();
        EntityQueryWithOptions queryWithOptions = Entities.suchThat()
                .systemName().eq(SYSTEM_NAME)
                .and().keyValues().like(cmwSystemSpec, Map.of(
                        "device", nameQuery, "property", "%"
                )).withOptions().noHistory().orderBy().id().asc().limit(10);
        return entityService.findAll(queryWithOptions);
    }

    List<Entity> queryAndLimitEntitiesWithHistory(String nameQuery) {
        SystemSpec cmwSystemSpec = systemSpecService.findByName(SYSTEM_NAME).orElseThrow();
        EntityQueryWithOptions queryWithOptions = Entities.suchThat()
                .systemName().eq(SYSTEM_NAME)
                .and().keyValues().like(cmwSystemSpec, Map.of(
                        "device", nameQuery, "property", "%"
                )).withOptions().orderBy().id().asc().limit(10);
        return entityService.findAll(queryWithOptions);
    }

    Set<Entity> queryEntities(String nameQuery) {
        SystemSpec cmwSystemSpec = systemSpecService.findByName(SYSTEM_NAME).orElseThrow();
        var query = Entities.suchThat()
                .systemName().eq(SYSTEM_NAME)
                .and().keyValues().like(cmwSystemSpec, Map.of(
                        "device", nameQuery, "property", "%"
                ));

        return entityService.findAll(query);
    }
}

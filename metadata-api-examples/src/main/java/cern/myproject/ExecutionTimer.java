package cern.myproject;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ExecutionTimer {
    private static final Logger log = LoggerFactory.getLogger(ExecutionTimer.class);

    @Around("execution(* cern.myproject.*Queries.query*(..))")
    public Object timeMeasure(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.nanoTime();
        Object retVal = pjp.proceed();
        long stop = System.nanoTime() - start;
        log.info("Method {} took {} s", pjp, stop / 1e9);
        return retVal;
    }
}

package cern.myproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetadataAccessExample.class)
public class EntityQueriesTest {
    private static final String QUERY_STRING = "SPS%";

    @Autowired
    private EntityQueries entityQueries;

    @Test
    public void shouldRetrieve10VariablesIfLimit() {
        assertEquals(10, entityQueries.queryAndLimitEntitiesWithHistory(QUERY_STRING).size());
        assertEquals(10, entityQueries.queryAndLimitEntitiesWithoutHistory(QUERY_STRING).size());
    }

    @Test
    public void shouldRetrieveEqualNumberOfVariablesDespiteQuery() {
        assertEquals(entityQueries.queryEntities(QUERY_STRING).size(), entityQueries.queryEntitiesWithoutHistory(QUERY_STRING).size());
    }
}

package cern.myproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetadataAccessExample.class)
public class VariableQueriesTest {
    private static final String QUERY_STRING = "SPS%";

    @Autowired
    private VariableQueries variableQueries;

    @Test
    public void shouldRetrieve10VariablesIfLimit() {
        assertEquals(10, variableQueries.queryAndLimitThickVariables(QUERY_STRING).size());
        assertEquals(10, variableQueries.queryAndLimitThinVariables(QUERY_STRING).size());
    }

    @Test
    public void shouldRetrieveEqualNumberOfVariablesDespiteQuery() {
        assertEquals(variableQueries.queryVariables(QUERY_STRING).size(), variableQueries.queryThinVariables(QUERY_STRING).size());
    }
}

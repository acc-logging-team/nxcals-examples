# NXCALS-EXAMPLES

> This repository contains example code, capable of featuring typical client interaction with the NXCALS service using differnt APIs.


1. Ingestion API (ingestion-api-examples)
2. Extraction API (extraction-api-examples)
3. Extration Thin API (extraction-api-thin-examples)
4. Backport API (backport-api-examples)


## How to get started



The above mentioned examples, are the product of an effort to make all the featured actions to **reflect
as much as possible a real show-case** of the NXCALS system. This means that, in order to get started and
run your first example, there are a couple of things to be addressed up-front. *Let's address them one-by-one* 

### Request access to NXCALS examples
Contact the team members via email to **```acc-logging-support@cern.ch```** and request access for the examples,
stating on the email content your CERN username. Our authorization system is based on each user's kerberos principal,
in order to authorize it's role based access. Once this is done, you will be granted with full access to **MOCK-SYSTEM**.

> **MOCK-SYSTEM**, is registered to NXCALS TESTBED environment and it's dedicated for the show-case domain of the examples.

### Download and install the CERN grid CA certificate
Follow the steps described by the following BE-CO Wikis page, on "Install CERN grid CA certificate" sector.

Link to wiki page [here](https://wikis.cern.ch/display/NXCALS/NXCALS+-+Data+Access+User+Guide#NXCALS-DataAccessUserGuide-InstallCERNgridCAcertificate)

Then modify/validate the 2 system properties defined on each example for loading the CA grid certificate.
Directly on the example code the corresponding properties are:

```java
    // please modify accordingly to your CA certificate entry on JDK keytool
    System.setProperty("javax.net.ssl.trustStore", "nxcals_cacerts");
    System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
```

### Point the example code to a valid kerberos keytab, configured for your principal.

>If you already have a valid kerberos keytab, they just update the 2 corresponding system properties, as featured bellow,
at the end of this sector.

Please follow the steps described by our data-access page on BE-CO Wikis, in order to create a kerberos keytab.

Link to wiki page [here](https://wikis.cern.ch/display/NXCALS/NXCALS+-+Data+Access+User+Guide#NXCALS-DataAccessUserGuide-InstallconfigureKerberosonyourmachine)

Now that you have successfully stored your keytab under a path on your filesystem, update the following properties to
point the examples to it:

```java
    //Very important to have a valid kerberos token.
    //Here we consider the the keybal is located under your home directory
    String USER_HOME = System.getProperty("user.home");
    // provide path to your keytab, by default it's set as follows
    System.setProperty("kerberos.keytab", USER_HOME + "/.keytab");
    // your user principal or a system account
    // you can skip the user assignment to directly point to a keytab of a service account
    String USER = System.getProperty("user.name");
    System.setProperty("kerberos.principal", USER);
```

Some of the examples here (Thin API) rely on RBAC and some on Kerberos (Extraction API).
Therefore you need both Kerberos token (obtained via a keytab file) and a user/password set.
This can be done with system properties given at the run command below. 

### Run 
```bash
../gradlew run -Dkerberos.keytab=<path to keytab file> -Dkerberos.principal=<user> -Duser.password=<CERN password> -Duser.name=<user>
```
You should see the spark output. 

### CBNG
This project uses gradle wrapper to build itself. 
If you still want to use CBNG build system please run the script: 

```bash
./convert-to-cbng.sh
```
It will change the config files of gradle so they do not clash with CBNG product.xml (apply the -no-cbng suffix to them). 
If you want to convert back to gradle run: 
```bash
./convert-from-cbng.sh
``` 

---

*With all the above in place, you're good to go and build/run the examples against NXCALS TESTBED environment.*